PREFIX=/usr/local
PREFIX_MAN=$(PREFIX)/man
CFLAGS?=-g -Wall

all: sutter

sutter: sutter.o main.o data.o utils.o render.o parallel.o
	$(CC) $(CFLAGS) sutter.o main.o data.o utils.o render.o parallel.o -pthread $(LDFLAGS) -o $@

.c.o:
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $<

clean:
	rm -rf *.o *.core sutter makefile.depend

dep:
	$(CC) -MM *.c > makefile.depend

install:
	mkdir -p -m 755 $(PREFIX)/bin
	install -m 755 sutter $(PREFIX)/bin/sutter
#	mkdir -p -m 755 $(PREFIX_MAN)/man1
#	install -m 644 doc/sutter.1 $(PREFIX_MAN)/man1/sutter.1
#	mkdir -p -m 755 $(PREFIX_MAN)/man5
#	install -m 644 doc/sutter.5 $(PREFIX_MAN)/man5/sutter.5

data.o: data.c xs.h xs_io.h xs_json.h xs_time.h xs_match.h xs_btree.h \
 sutter.h
main.o: main.c xs.h sutter.h
parallel.o: parallel.c xs.h sutter.h
render.o: render.c xs.h xs_io.h xs_regex.h xs_md5.h xs_time.h xs_mime.h \
 xs_html.h xs_unicode.h sutter.h
sutter.o: sutter.c xs.h xs_hex.h xs_io.h xs_unicode_tbl.h xs_unicode.h \
 xs_json.h xs_time.h xs_regex.h xs_md5.h xs_match.h xs_mime.h xs_btree.h \
 xs_html.h sutter.h
utils.o: utils.c xs.h xs_io.h xs_json.h sutter.h
