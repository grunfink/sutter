# Release Notes

## UNRELEASED

There is a new configuration paramter, `author_fediverse`, that is a string value in the format @account@server, that will be added as a `<meta name="fediverse:creator"...` tag in all HTML headers. This value can be set in `sutter.json` (default fediverse creator account), or in a post's metadata, for special writer guests.

There is a new entry configuration metadata, `discuss`, that contains a URL link to a site where discuss about an entry can happen (a Fediverse post, forum entry, etc.).

Fixed path for CSS includes (contributed by letterus).

Removed .gmi suffix for local .html links (contributed by letterus).

## 1.05

Added a final cleanup stage: all stale targets (i.e. all targets that were not generated in a session) are deleted.

Fixed a crash when an article has a `author_name` field but not an `author_avatar` one.

The number of post entries in the RSS file can be changed from the default 10 by setting the `num_rss_entries` field in the configuration file to an alternative number.

Fixed a bug in header numbering (it was one off).

The `2html` command-line option sets the output HTML title to the content of the first `<h1>` tag, if there is one.

Any URL schema is accept as valid (not only http/https).

## 1.04

The output from the `2html` command now includes the (mandatory these days) `meta name viewport` HTML tag crap.

When converting from Gemini to Gemini, don't add the document date if it's before *minimum_date* (like in Markdown conversions).

In Gemini to HTML conversion, inline references like [1] are converted to links if there is a Gemini link which label starts with the same [1] text.

Added Markdown support for strikethrough, italic+bold and images.

## 1.03

The index processing have been simplified and optimized.

When converting Markdown and Gemini sources to HTML, if a word follows the 3 backticks marker for the start of a preformatted block, it's used as a CSS class.

Headers from Markdown and Gemini sources now include an anchor and a permalink.

## 1.02

Added support for [label](url) Markdown links.

Other minor internal tweaks.

## 1.01

Converted RSS version from 0.9 to 2.0.

All images have now absolute paths.

The title, author and avatar subheader has been moved out of the `<article>` wrapper tags. This make it saner for RSS.

If an entry has a `keywords` metadata string, they are inserted as a meta tag in the HTML header page.

New command line option `check`, that iterates all entries and adds metadata to those that don't have it.

Markdown-like italic, bold and monospace markup is converted from Gemini to HTML.

Added meta:og properties to HTML output.

New command-line option `2html`, to convert Markdown or Gemini files directly to HTML without the need of a `sutter` infrastructure.

## 0.99

First public release.
