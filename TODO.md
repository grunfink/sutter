# TODO

## Open

## Closed

Move to data.c all code in index.c and delete it (2024-08-17T19:29:58+0200).

Don't use xs_list_pop() in the helper threads, by just a list context (2024-08-17T19:29:58+0200).

Move prepare_templates() call to sutter_open() (2024-08-17T19:29:58+0200).

Use a dict instead of a btree for the index (IMPOSSIBLE: a btree is used because it must be reverse-sorted; 2024-08-17T20:07:23+0200).

Implement cleanup: read all content in wwwdir and gmidir into a dict, delete each rendered or hardlinked element, and delete the rest of cruft (2024-09-05T13:49:37+0200).
