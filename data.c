/* sutter - A static blog generator */
/* copyright (c) 2023 - 2025 grunfink - MIT license */

#include "xs.h"
#include "xs_io.h"
#include "xs_json.h"
#include "xs_time.h"
#include "xs_match.h"
#include "xs_btree.h"

#include "sutter.h"

#include <sys/stat.h>
#include <glob.h>
#include <pthread.h>

/* the mutex to protect the btree */
static pthread_mutex_t btree_mutex = {0};

/* the mutex to protect the target dict */
static pthread_mutex_t targets_mutex = {0};

/** file utils **/

double mtime_nl(const char *fn, int *n_link)
/* returns the mtime and number of links of a file or directory, or 0.0 */
{
    struct stat st;
    double r = 0.0;
    int n = 0;

    if (fn && stat(fn, &st) != -1) {
        r = (double) st.st_mtim.tv_sec;
        n = st.st_nlink;
    }

    if (n_link)
        *n_link = n;

    return r;
}


xs_val *rglob(xs_val *store, const char *dir)
/* globs a tree, recursively (store can be a list or a dict) */
{
    glob_t globbuf;
    xs *spec = xs_dup(dir);

    if (xs_endswith(spec, "/"))
        spec = xs_str_cat(spec, "*");
    else
        spec = xs_str_cat(spec, "/" "*");

    if (glob(spec, GLOB_MARK, NULL, &globbuf) == 0) {
        int n;

        for (n = 0; n < (int) globbuf.gl_pathc; n++) {
            char *p = globbuf.gl_pathv[n];

            if (xs_endswith(p, "/"))
                store = rglob(store, p);
            else {
                if (xs_type(store) == XSTYPE_DICT)
                    store = xs_dict_set(store, p, p);
                else
                    store = xs_list_append(store, p);
            }
        }
    }

    globfree(&globbuf);

    return store;
}


void create_full_path(const char *fn)
/* creates the full path to fn */
{
    xs *l = xs_split(fn, "/");
    xs *t = xs_str_new(NULL);
    xs_val *bn = NULL;
    const xs_val *v;

    /* drop the basename */
    l = xs_list_pop(l, &bn);
    bn = xs_free(bn);

    xs_list_foreach(l, v) {
        t = xs_str_cat(t, v, "/");

        mkdir(t, 0755);
    }
}


int write_file_if_diff(const char *fn, const char *content)
/* writes content into the filename if it's different */
{
    FILE *f;
    int diff = 1;
    struct stat st;

    if (stat(fn, &st) != -1) {
        int sz = strlen(content);

        if (sz == st.st_size) {
            /* same size: read by blocks and compare */
            if ((f = fopen(fn, "r")) != NULL) {
                char buf[32768];
                int offset = 0;
                int n;

                diff = 0;

                while (offset < sz && (n = fread(buf, 1, sizeof(buf), f))) {
                    if (strncmp(content + offset, buf, n) != 0) {
                        /* block is different */
                        diff = 1;
                        break;
                    }

                    offset += n;
                }

                fclose(f);
            }
        }
    }

    if (diff) {
        create_full_path(fn);

        if ((f = fopen(fn, "w")) != NULL) {
            fwrite(content, strlen(content), 1, f);
            fclose(f);
        }
        else
            diff = -1;
    }

    return diff;
}


xs_dict *entry_load(sutter *sutter, const char *fn)
/* loads an entry */
{
    FILE *f;
    int has_meta = 0;
    int has_date = 0;
    int has_title = 0;

    if (!xs_match(fn, "*.html|*.raw_html|*.md|*.gmi|*.raw_gmi"))
        return NULL;

    if ((f = fopen(fn, "r")) == NULL) {
        sutter_log(sutter, xs_fmt("cannot open '%s' for reading", fn));
        return NULL;
    }

    xs_dict *entry = xs_dict_new();
    xs *content    = xs_list_new();

    /* read the body */
    while (!feof(f)) {
        xs *l = xs_readline(f);

        if (l == NULL)
            break;

        l = xs_strip_chars_i(l, "\r\n");

        if (strcmp(l, "__META__") == 0) {
            has_meta = 1;
            break;
        }

        content = xs_list_append(content, l);
    }

    entry = xs_dict_set(entry, "content", content);
    entry = xs_dict_set(entry, "_fn",     fn);

    xs *id = xs_replace(fn, sutter->srcdir, "");

    char *p = strrchr(id, '.');
    *p = '\0';

    entry = xs_dict_set(entry, "_id", id);

    /* calculate the prefix (the directory where the entry lives) */
    xs *m = xs_split(id, "/");
    xs *d = NULL;

    m = xs_list_pop(m, &d);
    xs *prefix = xs_join(m, "/");

    entry = xs_dict_set(entry, "_prefix", prefix);

    /* read the metadata */
    while (!feof(f)) {
        xs *l = xs_readline(f);

        if (l == NULL || *l == '\0')
            break;

        xs *m = xs_split_n(l, ":", 1);

        if (xs_list_len(m) == 2) {
            xs *k = xs_tolower_i(xs_dup(xs_list_get(m, 0)));
            xs *v = xs_strip_i(xs_dup(xs_list_get(m, 1)));

            if (strcmp(k, "date") == 0)
                has_date = 1;
            if (strcmp(k, "title") == 0)
                has_title = 1;

            if (strcmp(k, "keywords") == 0) {
                /* split */
                xs *kw_list = xs_list_new();
                xs *kw_spl = xs_split(v, ",");
                const char *v;

                xs_list_foreach(kw_spl, v) {
                    xs *s = xs_strip_i(xs_dup(v));
                    kw_list = xs_list_append(kw_list, s);
                }

                entry = xs_dict_set(entry, k, kw_list);
            }
            else
                entry = xs_dict_set(entry, k, v);
        }
        else
            printf("[%s]\n", l);
    }

    fclose(f);

    /* fix lacking things in the source file */
    if (has_meta + has_date + has_title < 3) {
        time_t ft = (time_t) mtime(fn);

        if ((f = fopen(fn, "a")) == NULL) {
            sutter_log(sutter, xs_fmt("cannot open '%s' for append", fn));
            entry = xs_free(entry);
        }
        else {
            if (!has_meta) {
                const char *ll = xs_list_get(content, -1);

                /* if the last line of the content is not empty, add a newline */
                if (ll && strlen(ll))
                    fprintf(f, "\n");

                fprintf(f, "__META__\n");
            }

            if (!has_date) {
                xs *date = xs_str_utctime(ft, "%Y-%m-%dT%H:%M:%SZ");
                entry = xs_dict_set(entry, "date", date);
                fprintf(f, "date: %s\n", date);

                entry = xs_dict_set(entry, "date", date);
            }

            if (!has_title) {
                xs *l     = xs_split(fn, "/");
                xs *title = xs_dup(xs_list_get(l, -1));
                char *p   = strrchr(title, '.');

                if (p)
                    *p = '\0';

                title    = xs_replace_i(title, "-", " ");
                title    = xs_replace_i(title, "_", " ");
                title[0] = toupper(title[0]);

                fprintf(f, "title: %s\n", title);

                entry = xs_dict_set(entry, "title", title);
            }

            fclose(f);

            sutter_debug(sutter, 1, xs_fmt("added metadata to '%s'", fn));
        }
    }

    return entry;
}


void index_add(sutter *sutter, const xs_dict *entry, const char *fn)
/* adds an entry to the index */
{
    const char *date  = xs_dict_get(entry, "date");
    const char *title = xs_dict_get(entry, "title");

    xs *key = xs_fmt("%s\t%s\t%s", date, title, fn);

    pthread_mutex_lock(&btree_mutex);

    sutter->index = xs_btree_set(sutter->index, key, "");

    pthread_mutex_unlock(&btree_mutex);
}


xs_list *index_read(sutter *sutter)
/* returns the index, reverse sorted */
{
    xs_btree_iter i = xs_btree_iter_new(sutter->index, -1);
    const xs_btree *v;
    xs_list *entries = xs_list_new();

    while ((v = xs_btree_iter_next(&i)))
        entries = xs_list_append(entries, xs_btree_key(v));

    return entries;
}


void delete_target(sutter *sutter, const char *fn)
/* deletes a filename from the list of targets */
{
    pthread_mutex_lock(&targets_mutex);
    sutter->targets = xs_dict_del(sutter->targets, fn);
    pthread_mutex_unlock(&targets_mutex);
}


void target_cleanup(sutter *sutter)
/* deletes all stale target files that were not generated in this session */
{
    const char *k;
    const char *v;

    xs_dict_foreach(sutter->targets, k, v) {
        unlink(k);
        sutter_log(sutter, xs_fmt("deleted %s", k));
    }
}


int sutter_open(sutter *sutter, const char *basedir)
/* opens a sutter data storage */
{
    int ret = 0;
    xs *cfg_file = NULL;
    FILE *f;
    xs_str *error = NULL;

    memset(sutter, '\0', sizeof(struct _sutter));

    sutter->basedir = xs_rstrip_chars_i(xs_dup(basedir), "/");

    cfg_file = xs_fmt("%s/sutter.json", sutter->basedir);

    if ((f = fopen(cfg_file, "r")) == NULL)
        error = xs_fmt("ERROR: cannot open '%s'", cfg_file);
    else {
        /* parse */
        sutter->config = xs_json_load(f);
        fclose(f);

        if (sutter->config == NULL)
            error = xs_fmt("ERROR: cannot parse '%s'", cfg_file);
        else {
            const xs_val *dbglvl = xs_dict_get(sutter->config, "dbglevel");

            sutter->dbglevel = (int) xs_number_get(dbglvl);

            if ((dbglvl = getenv("DEBUG")) != NULL) {
                sutter->dbglevel = atoi(dbglvl);
                error = xs_fmt("DEBUG level set to %d from environment", sutter->dbglevel);
            }

            ret = 1;
        }
    }

    if (error != NULL)
        sutter_log(sutter, error);

    if (ret == 0)
        return ret;

    /* expand the sub directories */
    sutter->srcdir = xs_fmt("%s/%s", sutter->basedir, xs_dict_get(sutter->config, "srcdir"));
    sutter->wwwdir = xs_fmt("%s/%s", sutter->basedir, xs_dict_get(sutter->config, "wwwdir"));
    sutter->gmidir = xs_fmt("%s/%s", sutter->basedir, xs_dict_get(sutter->config, "gmidir"));

    sutter->ok = ret;

    prepare_templates(sutter);

    /* read all sources */
    sutter->sources = rglob(xs_list_new(), sutter->srcdir);
    sutter->sources_ctx = 0;

    /* read all current targets */
    sutter->targets = rglob(xs_dict_new(), sutter->wwwdir);
    sutter->targets = rglob(sutter->targets, sutter->gmidir);

    return ret;
}


void sutter_close(sutter *sutter)
/* closes and frees everything */
{
#if 0
    xs_free(sutter->basedir);
    xs_free(sutter->config);
    xs_free(sutter->srcdir);
    xs_free(sutter->wwwdir);
    xs_free(sutter->gmidir);
    xs_free(sutter->sources);

    memset(sutter, '\0', sizeof(struct _sutter));
#endif
}
