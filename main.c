/* sutter - A static blog generator */
/* copyright (c) 2023 - 2025 grunfink - MIT license */

#include "xs.h"

#include "sutter.h"

int usage(void)
{
    printf("sutter " VERSION " - A static blog generator\n");
    printf("Copyright (c) 2023 - 2025 grunfink - MIT license\n");
    printf("\n");
    printf("Commands:\n");
    printf("\n");
    printf("init [{basedir}]    Initializes the data storage\n");
    printf("run {basedir}       Builds the site in parallel (multithreaded) mode\n");
    printf("srun {basedir}      Builds the site in serial (1 thread) mode\n");
    printf("check {basedir}     Ensures that all entries have their metadata\n");
    printf("2html {file}        Renders a Markdown or Gemini file to HTML\n");

    return 1;
}


char *get_argv(int *argi, int argc, char *argv[])
{
    if (*argi < argc)
        return argv[(*argi)++];
    else
        return NULL;
}


#define GET_ARGV() get_argv(&argi, argc, argv)

int main(int argc, char *argv[])
{
    char *cmd;
    char *basedir;
    int argi = 1;
    sutter sutter;

    if ((cmd = GET_ARGV()) == NULL)
        return usage();

    if (strcmp(cmd, "init") == 0) {
        /* initialize the database */
        /* ... */
        basedir = GET_ARGV();

        return sutter_init(basedir);
    }

    if ((basedir = GET_ARGV()) == NULL)
        return usage();

    if (strcmp(cmd, "2html") == 0) {
        /* raw render to html: basedir is really a file */
        int r = file_render_to_www(basedir);
        if (r != 0)
            fprintf(stderr, "ERROR: cannot convert '%s' to html\n", basedir);

        return r;
    }

    if (!sutter_open(&sutter, basedir)) {
        sutter_log(&sutter, xs_fmt("error opening data storage at %s", basedir));
        return 1;
    }

    if (strcmp(cmd, "run") == 0) {
        render_all_parallel(&sutter);
        sutter_close(&sutter);
        return 0;
    }

    if (strcmp(cmd, "srun") == 0) {
        render_all_serial(&sutter);
        sutter_close(&sutter);
        return 0;
    }

    if (strcmp(cmd, "check") == 0) {
        /* load all entries */
        xs *site_entry_ids = rglob(xs_list_new(), sutter.srcdir);
        const xs_val *v;

        xs_list_foreach(site_entry_ids, v) {
            xs_dict *entry = entry_load(&sutter, v);

            entry = xs_free(entry);
        }

        sutter_close(&sutter);
        return 0;
    }

    return usage();
}
