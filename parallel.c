/* sutter - A static blog generator */
/* copyright (c) 2023 - 2025 grunfink - MIT license */

#include "xs.h"

#include "sutter.h"

#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <fcntl.h>

/* mutex to access wrk_list */
static pthread_mutex_t wrk_list_mutex;

/* semaphore to access wrk_list */
static sem_t *wrk_list_sem = NULL;


/** code **/

static void *working_thread(void *arg)
/* processes entries from wrk_list */
{
    sutter *sutter;

    sutter = (struct _sutter *)arg;

    sutter_debug(sutter, 1, xs_fmt("thread started"));

    while (sem_wait(wrk_list_sem) == 0) {
        const xs_dict *entry_fn = NULL;

        /* get next in list */
        pthread_mutex_lock(&wrk_list_mutex);
        xs_list_next(sutter->sources, &entry_fn, &sutter->sources_ctx);
        pthread_mutex_unlock(&wrk_list_mutex);

        /* no more entries? done */
        if (entry_fn == NULL)
            break;

        entry_process(sutter, entry_fn);
    }

    sutter_debug(sutter, 1, xs_fmt("thread finished"));

    return NULL;
}


#define MAX_THREADS 256

void render_all_parallel(sutter *sutter)
/* renders everything with multiple thread parallelization */
{
    pthread_t threads[MAX_THREADS];
    int n_threads, n;
    xs *sem_name = xs_fmt("/sutter_job_%d", getpid());
    sem_t anon_job_sem;

#ifdef _SC_NPROCESSORS_ONLN
    n_threads = sysconf(_SC_NPROCESSORS_ONLN);
#else
    n_threads = 4; // reasonable default?
#endif

    if (n_threads > MAX_THREADS)
        n_threads = MAX_THREADS;

    sutter_debug(sutter, 1, xs_fmt("parallelizing with %d threads", n_threads));

    /* init the structures */
    pthread_mutex_init(&wrk_list_mutex, NULL);

    if ((wrk_list_sem = sem_open(sem_name, O_CREAT, 0644, 0)) == NULL) {
        /* error opening a named semaphore; try with an anonymous one */
        if (sem_init(&anon_job_sem, 0, 0) != -1)
            wrk_list_sem = &anon_job_sem;
    }

    if (wrk_list_sem == NULL) {
        sutter_log(sutter, xs_fmt("fatal error: cannot create semaphore -- cannot continue"));
        return;
    }

    /* launch the threads */
    for (n = 0; n < n_threads; n++)
        pthread_create(&threads[n], NULL, working_thread, sutter);

    /* post to the semaphore for every element to process
       and then one more per thread once the list is empty */
    for (n = xs_list_len(sutter->sources) + n_threads; n > 0; n --)
        sem_post(wrk_list_sem);

    /* wait for all threads to end */
    for (n = 0; n < n_threads; n++)
        pthread_join(threads[n], NULL);

    sem_close(wrk_list_sem);
    sem_unlink(sem_name);

    render_indexes(sutter);

    target_cleanup(sutter);
}
