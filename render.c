/* sutter - A static blog generator */
/* copyright (c) 2023 - 2025 grunfink - MIT license */

#include "xs.h"
#include "xs_io.h"
#include "xs_regex.h"
#include "xs_md5.h"
#include "xs_time.h"
#include "xs_mime.h"
#include "xs_html.h"
#include "xs_unicode.h"

#include "sutter.h"

#include <sys/stat.h>

const char *html_page_template = ""
    "<!DOCTYPE html>\n"
    "<head>\n"
    "<!-- css -->"
    "<link rel=\"alternate\" type=\"application/rss+xml\" "
    "title=\"RSS\" href=\"<!-- baseurl -->/rss.xml\"/>\n"
    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>\n"
    "<!-- keywords -->"
    "<meta name=\"generator\" content=\"sutter\"/>\n"
    "<meta property=\"og:site_name\" content=\"<!-- blog_title -->\"/>\n"
    "<meta property=\"og:title\" content=\"<!-- entry_title -->\"/>\n"
    "<meta property=\"og:description\" content=\"<!-- author_name -->: <!-- entry_title -->\"/>\n"
    "<meta property=\"og:image\" content=\"<!-- author_avatar -->\"/>\n"
    "<meta property=\"og:image:width\" content=\"300\"/>\n"
    "<meta property=\"og:image:height\" content=\"300\"/>\n"
    "<!-- more_meta -->"
    "<title><!-- blog_title -->: <!-- entry_title --></title>\n"
    "<body>\n"
    "<nav><a href=\"<!-- baseurl -->/\"><!-- blog_title --></a></nav>\n"
    "<h1 class=\"p-name\"><!-- entry_title --></h1>\n"
    "<!-- author_header -->"
    "<article class=\"h-entry\" lang=\"<!-- lang -->\">\n"
    "<div class=\"e-content\">\n"
"";

const char *html_page_footer = ""
    "</div>\n"
    "</article>\n"
    "</body>\n"
    "</html>\n"
"";

xs_str *html_page = NULL;

const char *html_author_header_template = ""
    "<p><img class=\"sutter-avatar\" src=\"<!-- author_avatar -->\"/>\n"
    "<span class=\"p-author h-card sutter-author\"><!-- author_name --></span><br/>\n"
    "<time class=\"dt-publised sutter-pubdate\" "
    "datetime=\"<!-- pubdate -->\"><!-- short_pubdate --></time></p>\n"
"";

const char *gemini_footer_template = ""
    "\n=> ../index.gmi <!-- blog_title -->\n"
"";

xs_str *gemini_footer = NULL;


/** code **/

void prepare_templates(sutter *sutter)
/* builds the header and footer */
{
    FILE *f;
    const xs_list *p;
    const xs_str *baseurl;
    const xs_str *title;
    const xs_val *v;

    html_page     = xs_str_new(html_page_template);
    gemini_footer = xs_str_new(gemini_footer_template);

    baseurl = xs_dict_get(sutter->config, "baseurl");
    html_page     = xs_replace_i(html_page,     "<!-- baseurl -->", baseurl);
    gemini_footer = xs_replace_i(gemini_footer, "<!-- baseurl -->", baseurl);

    title = xs_dict_get(sutter->config, "title");
    html_page     = xs_replace_i(html_page,     "<!-- blog_title -->", title);
    gemini_footer = xs_replace_i(gemini_footer, "<!-- blog_title -->", title);

    xs *css = NULL;
    xs *css_fn = xs_fmt("%s/sutter.css", sutter->basedir);

    if ((f = fopen(css_fn, "r")) != NULL) {
        css = xs_readall(f);
        fclose(f);

        css = xs_str_wrap_i("<style>\n", css, "</style>\n");
    }
    else
        css = xs_str_new(NULL);

    p = xs_dict_get(sutter->config, "cssurls");
    xs_list_foreach(p, v) {
        xs *s1 = xs_fmt("<link rel=\"stylesheet\" type=\"text/css\" href=\"%s/%s\"/>\n", baseurl, v);
        css = xs_str_cat(css, s1);
    }

    html_page = xs_replace_i(html_page, "<!-- css -->", css);
}


static int sutter_write_entry(sutter *sutter, const xs_dict *entry, const char *fn, const char *content)
/* writes an entry if it's different, logging what happened */
{
    int diff = write_file_if_diff(fn, content);

    switch (diff) {
    case 0:
        sutter_debug(sutter, 1, xs_fmt("not touching %s", fn));
        break;
    case 1:
        sutter_log(sutter, xs_fmt("written %s", fn));
        break;
    case -1:
        sutter_log(sutter, xs_fmt("cannot write %s", fn));
        break;
    }

    if (diff >= 0)
        index_add(sutter, entry, fn);

    delete_target(sutter, fn);

    return diff;
}


static xs_str *_markdown_line(xs_str *line, int url)
/* processes line-level markup */
{
    xs_str *s = xs_str_new(NULL);
    const xs_val *v;

    /* split by markup */
    xs *sm = xs_regex_split(line,
        "("
        "`[^`]+`"                           "|"
        "~~[^~]+~~"                         "|"
        "\\*\\*?\\*?[^\\*]+\\*?\\*?\\*"     "|"
        "[a-z]+:/" "/[^ \t,;:<]+"           "|"
        "!\\[[^]]*\\]\\([^\\)]+\\)"         "|"
        "\\[[^]]*\\]\\([^\\)]+\\)"
        ")");
    int n = 0;

    xs_list_foreach(sm, v) {
        if ((n & 0x1)) {
            /* markup */
            if (xs_startswith(v, "`")) {
                xs *s1 = xs_strip_chars_i(xs_dup(v), "`");
                s1 = xs_str_wrap_i("<code>", s1, "</code>");
                s = xs_str_cat(s, s1);
            }
            else
            if (xs_startswith(v, "~~")) {
                xs *s1 = xs_strip_chars_i(xs_dup(v), "~");
                s1 = xs_str_wrap_i("<s>", s1, "</s>");
                s = xs_str_cat(s, s1);
            }
            else
            if (xs_startswith(v, "***")) {
                xs *s1 = xs_strip_chars_i(xs_dup(v), "*");
                s1 = xs_str_wrap_i("<b><i>", s1, "</i></b>");
                s = xs_str_cat(s, s1);
            }
            else
            if (xs_startswith(v, "**")) {
                xs *s1 = xs_strip_chars_i(xs_dup(v), "*");
                s1 = xs_str_wrap_i("<b>", s1, "</b>");
                s = xs_str_cat(s, s1);
            }
            else
            if (xs_startswith(v, "*")) {
                xs *s1 = xs_strip_chars_i(xs_dup(v), "*");
                s1 = xs_str_wrap_i("<i>", s1, "</i>");
                s = xs_str_cat(s, s1);
            }
            else
            if (xs_startswith(v, "[")) {
                /* dumb [label](uri) Markdown links */
                xs *w    = xs_strip_chars_i(xs_dup(v), "[)");
                xs *l    = xs_split(w, "](");
                const char *label = xs_list_get(l, 0);
                const char *url   = xs_list_get(l, 1);
                xs *link = xs_fmt("<a href=\"%s\">%s</a>",
                            url, *label ? label : url);

                s = xs_str_cat(s, link);
            }
            else
            if (xs_startswith(v, "![")) {
                /* dumb ![label](uri) Markdown images */
                xs *w    = xs_strip_chars_i(xs_dup(v), "![)");
                xs *l    = xs_split(w, "](");
                xs *link = NULL;
                const char *alt_text = xs_list_get(l, 0);
                const char *img_url  = xs_list_get(l, 1);

                if (*alt_text) {
                    link = xs_fmt("<figure><img src=\"%s\" alt=\"%s\"/>"
                        "<figcaption>%s</figcaption></figure>",
                        img_url, alt_text, alt_text);
                }
                else {
                    link = xs_fmt("<img src=\"%s\" alt=\"\"/>", img_url);
                }

                s = xs_str_cat(s, link);
            }
            else
            if (xs_str_in(v, ":/" "/") != -1) {
                /* url: 0: pass through; 1: make link */
                if (url) {
                    xs *u    = xs_strip_chars_i(xs_dup(v), ".");
                    xs *link = xs_fmt("<a href=\"%s\">%s</a>", u, v);
                    s = xs_str_cat(s, link);
                }
                else
                    s = xs_str_cat(s, v);
            }
            else
                s = xs_str_cat(s, v);
        }
        else
            /* surrounded text, copy directly */
            s = xs_str_cat(s, v);

        n++;
    }

    xs_free(line);

    return s;
}


int url_is_image(const char *url)
{
    const char *mime_type = xs_mime_by_ext(url);

    return xs_startswith(mime_type, "image/");
}


xs_str *make_slug(const char *text)
/* builds a slug */
{
    xs_str *s = xs_str_new(NULL);
    unsigned int cpoint;

    /* iterate the text by UTF-8 chars */
    while ((cpoint = xs_utf8_dec(&text))) {
        /* convert to lowercase */
        cpoint = xs_unicode_to_lower(cpoint);

        unsigned int diac;

        /* convert to nfd */
        xs_unicode_nfd(cpoint, &cpoint, &diac);

        /* keep only ascii alnum */
        if (cpoint > 127 || !isalnum(cpoint))
            cpoint = '-';

        /* add to the string, dropping the possible diacritic */
        s = xs_utf8_cat(s, cpoint);
    }

    /* strip surrounding hyphens */
    s = xs_strip_chars_i(s, "-");

    /* convert redundant hyphens */
    s = xs_regex_replace_i(s, "-{2,}", "-");

    return s;
}


xs_str *make_html_header(xs_str *text, int level)
/* makes a header of a given level, including an anchor */
{
    xs *t = xs_strip_chars_i(text, "# ");
    xs *h = xs_fmt("h%d", level);
    xs *s = make_slug(t);
    xs *s2 = xs_fmt("#%s", s);

    xs_html *header = xs_html_tag(h,
        xs_html_tag("a",
            xs_html_attr("name", s),
            xs_html_attr("href", s2),
            xs_html_attr("class", "permalink"),
            xs_html_text(t)));

    return xs_html_render(header);
}


static xs_str *_html_refs_to_links(xs_str *lc, const xs_list *links)
/* converts [n] references to html links */
{
    const char *v;

    /* join for easier replacing */
    xs *html = xs_join(lc, "\f");

    xs_list_foreach(links, v) {
        const char *url   = xs_list_get(v, 0);
        const char *label = xs_list_get(v, 1);

        if (url == NULL || label == NULL)
            continue;

        /* references must be [something] */
        if (!xs_regex_match(label, "^\\[.+\\] "))
            continue;

        xs *l = xs_split_n(label, " ", 1);
        const char *ref = xs_list_get(l, 0);

        /* the original link */
        xs *org_link = xs_fmt("<a href=\"%s\">%s</a>", url, label);

        /* the md5 (to save the original link) */
        xs *org_link_md5 = xs_md5_hex(org_link, strlen(org_link));

        /* the new link */
        xs *new_link = xs_fmt("<a href=\"%s\" title=\"%s\"><sup>%s</sup></a>", url, label, ref);

        /* replace first the original link to avoid re-replacing */
        html = xs_replace_i(html, org_link, org_link_md5);

        /* replace now the reference with the new link */
        html = xs_replace_i(html, ref, new_link);

        /* replace back the original link */
        html = xs_replace_i(html, org_link_md5, org_link);
    }

    xs_free(lc);

    return xs_split(html, "\f");
}


static xs_list *_gemini_to_html(const xs_list *content, const char *baseurl, const char *prefix)
/* converts a Gemini content to HTML */
{
    xs_list *nc = xs_list_new();
    const xs_val *v;
    enum { MODE_LINE, MODE_PRE, MODE_UL, MODE_BLQ } mode = MODE_LINE;
    xs *links = xs_list_new();

    xs_list_foreach(content, v) {
        xs *l = xs_dup(v);

        l = xs_replace_i(l, "<", "&lt;");

again:
        switch (mode) {
        case MODE_PRE:
            if (xs_startswith(l, "```")) {
                nc = xs_list_append(nc, "</pre>");
                mode = MODE_LINE;
            }
            else
                nc = xs_list_append(nc, l);

            break;

        case MODE_UL:
            if (!xs_startswith(l, "*")) {
                nc = xs_list_append(nc, "</ul>");
                mode = MODE_LINE;
                goto again;
            }

            l = xs_str_wrap_i("<li>", xs_strip_chars_i(l, "* "), "</li>");

            nc = xs_list_append(nc, l);

            break;

        case MODE_BLQ:
            if (l[0]) {
                if (!xs_startswith(l, ">")) {
                    nc = xs_list_append(nc, "</blockquote>");
                    mode = MODE_LINE;
                    goto again;
                }

                l = xs_str_wrap_i("<p>", xs_strip_chars_i(l, "> "), "</p>");
            }

            nc = xs_list_append(nc, l);

            break;

        case MODE_LINE:
            if (xs_startswith(l, ">")) {
                nc = xs_list_append(nc, "<blockquote>");
                mode = MODE_BLQ;
                goto again;
            }
            else
            if (xs_startswith(l, "*")) {
                nc = xs_list_append(nc, "<ul>");
                mode = MODE_UL;
                goto again;
            }
            else
            if (xs_startswith(l, "```")) {
                xs *clss = xs_replace(l, "```", "");
                xs *s1 = NULL;

                if (*clss)
                    s1 = xs_fmt("<pre class=\"%s\">", clss);
                else
                    s1 = xs_dup("<pre>");

                nc = xs_list_append(nc, s1);

                mode = MODE_PRE;
                continue;
            }
            else
            if (xs_startswith(l, "###")) {
                l = make_html_header(l, 3);
            }
            else
            if (xs_startswith(l, "##")) {
                l = make_html_header(l, 2);
            }
            else
            if (xs_startswith(l, "#")) {
                l = make_html_header(l, 1);
            }
            else
            if (xs_startswith(l, "=>")) {
                l = xs_strip_i(xs_replace_i(l, "=>", ""));
                xs *m = xs_split_n(l, " ", 1);

                xs_free(l);

                xs *url = xs_dup(xs_list_get(m, 0));

                int img = url_is_image(url);

                if (!img && xs_str_in(url, ":/" "/") == -1) {
                    /* if it has no protocol, it's local: add an extension */
                    /* Remove the .gmi extension before */
                    url = xs_replace_in(url, ".gmi", "", 1);
                    url = xs_str_cat(url, ".html");
                }

                {
                    /* store the link */
                    xs *l2 = xs_list_new();
                    l2 = xs_list_append(l2, url);
                    l2 = xs_list_append(l2, xs_list_get(m, 1));

                    links = xs_list_append(links, l2);
                }

                if (img) {
                    const char *caption = xs_list_len(m) == 1 ? "" : xs_list_get(m, 1);

                    l = xs_fmt("<figure>\n<img src=\"%s%s/%s\" alt=\"%s\"/>"
                               "<figcaption>%s</figcaption>\n</figure>\n",
                                baseurl, prefix, url, caption, caption);
                }
                else
                    l = xs_fmt("<a href=\"%s\">%s</a><br/>",
                        url, xs_list_len(m) == 1 ? url : xs_list_get(m, 1));
            }
            else {
                if (l[0])
                    l = xs_str_wrap_i("<p>", l, "</p>");
            }

            l = _markdown_line(l, 0);
            nc = xs_list_append(nc, l);

            break;
        }
    }

    /* convert possible link references */
    nc = _html_refs_to_links(nc, links);

    return nc;
}


static xs_list *_gemini_to_gemini(const xs_list *content)
/* filters gemini content */
{
    xs_list *nc = xs_list_new();
    const xs_val *v;

    xs_list_foreach(content, v) {
        if (xs_startswith(v, "=>")) {
            xs *m = xs_split_n(v, " ", 2);
            xs *url = xs_dup(xs_list_get(m, 1));

            int img = url_is_image(url);

            if (!img && xs_str_in(url, ":/" "/") == -1) {
                /* if it has no protocol, it's local */
                url = xs_str_cat(url, ".gmi");

                m = xs_list_set(m, 1, url);
            }

            xs *s = xs_join(m, " ");
            nc = xs_list_append(nc, s);
        }
        else
        if (xs_startswith(v, "```")) {
            /* ignore possible classes */
            nc = xs_list_append(nc, "```");
        }
        else
            nc = xs_list_append(nc, v);
    }

    return nc;
}


static xs_list *_markdown_to_html(const xs_list *content, int url)
/* converts a Markdown content to HTML */
{
    xs_list *nc = xs_list_new();
    const xs_val *v;
    enum { MODE_LINE, MODE_PRE, MODE_UL, MODE_BLQ,
           MODE_TBL_HDR, MODE_TBL_SEP, MODE_TBL_BODY } mode = MODE_LINE;
    xs *p_line = xs_str_new(NULL);

    xs_list_foreach(content, v) {
        xs *l = xs_dup(v);

        l = xs_replace_i(l, "<", "&lt;");

again:
        switch (mode) {
        case MODE_PRE:
            if (xs_startswith(l, "```")) {
                nc = xs_list_append(nc, "</pre>");
                mode = MODE_LINE;
            }
            else
                nc = xs_list_append(nc, l);

            break;

        case MODE_UL:
            if (!xs_startswith(l, "-")) {
                nc = xs_list_append(nc, "</ul>");
                mode = MODE_LINE;
                goto again;
            }

            l = xs_str_wrap_i("<li>", xs_strip_chars_i(l, "- "), "</li>");

            l = _markdown_line(l, url);

            nc = xs_list_append(nc, l);

            break;

        case MODE_BLQ:
            if (l[0]) {
                if (!xs_startswith(l, ">")) {
                    nc = xs_list_append(nc, "</blockquote>");
                    mode = MODE_LINE;
                    goto again;
                }

                l = xs_str_wrap_i("<p>", xs_strip_chars_i(l, "> "), "</p>");
            }

            l = _markdown_line(l, url);

            nc = xs_list_append(nc, l);

            break;

        case MODE_TBL_HDR:

            {
                nc = xs_list_append(nc, "<table>");

                l = xs_strip_chars_i(l, "|");
                xs *row = xs_split(l, "|");
                const char *v;

                nc = xs_list_append(nc, "<tr>");

                xs_list_foreach(row, v) {
                    xs *cell = xs_strip_i(xs_dup(v));
                    nc = xs_list_append(nc, "<th>", cell, "</th>");
                }

                nc = xs_list_append(nc, "</tr>");

                mode = MODE_TBL_SEP;
            }

            break;

        case MODE_TBL_SEP:

            /* just ignore the line and possible formatting (by now) */
            mode = MODE_TBL_BODY;

            break;

        case MODE_TBL_BODY:

            if (!xs_between("|", l, "|")) {
                /* end of table */
                nc = xs_list_append(nc, "</table>");
                mode = MODE_LINE;
                goto again;
            }
            else {
                l = xs_strip_chars_i(l, "|");
                xs *row = xs_split(l, "|");
                const char *v;

                nc = xs_list_append(nc, "<tr>");

                xs_list_foreach(row, v) {
                    xs *cell = xs_strip_i(xs_dup(v));
                    nc = xs_list_append(nc, "<td>", cell, "</td>");
                }

                nc = xs_list_append(nc, "</tr>");
            }

            break;

        case MODE_LINE:
            if (xs_startswith(l, ">")) {
                nc = xs_list_append(nc, "<blockquote>");
                mode = MODE_BLQ;
                goto again;
            }
            else
            if (xs_startswith(l, "-")) {
                nc = xs_list_append(nc, "<ul>");
                mode = MODE_UL;
                goto again;
            }
            else
            if (xs_startswith(l, "```")) {
                xs *clss = xs_replace(l, "```", "");
                xs *s1 = NULL;

                if (*clss)
                    s1 = xs_fmt("<pre class=\"%s\">", clss);
                else
                    s1 = xs_dup("<pre>");

                nc = xs_list_append(nc, s1);

                mode = MODE_PRE;
                continue;
            }
            else
            if (xs_startswith(l, "###")) {
                l = make_html_header(l, 3);
            }
            else
            if (xs_startswith(l, "##")) {
                l = make_html_header(l, 2);
            }
            else
            if (xs_startswith(l, "#")) {
                l = make_html_header(l, 1);
            }
            else
            if (xs_between("|", l, "|")) {
                mode = MODE_TBL_HDR;
                goto again;
            }
            else {
                if (l[0]) {
                    if (*p_line)
                        p_line = xs_str_cat(p_line, " ");

                    p_line = xs_str_cat(p_line, l);
                    l = xs_free(l);
                }
                else {
                    l = xs_str_cat(l, "<p>", p_line, "</p>");

                    free(p_line);
                    p_line = xs_str_new(NULL);
                }
            }

            if (l) {
                l = _markdown_line(l, url);

                nc = xs_list_append(nc, l);
            }

            break;
        }
    }

    if (*p_line) {
        p_line = xs_str_wrap_i("<p>", p_line, "</p>");
        nc = xs_list_append(nc, p_line);
    }

    return nc;
}


xs_list *special_uris(sutter *sutter, const xs_list *content, const char *prefix)
/* resolve special URIs from a list of content */
{
    xs_list *nc = xs_list_new();
    const xs_val *v;

    xs_list_foreach(content, v) {
        int n = 0;
        xs *l = xs_regex_split(v,
            "("
            "img:/" "/[^ \t<]+|"
            "thumb:/" "/[^ \t<]+|"
            "story:/" "/[A-Za-z0-9_-]+/[A-Za-z0-9_-]+ \\([^\\)]+\\)|"
            "story:/" "/[A-Za-z0-9_-]+/[A-Za-z0-9_-]+|"
            "body:/" "/[A-Za-z0-9_-]+/[A-Za-z0-9_-]+|"
            "topic:/" "/[A-Za-z0-9_-]+|"
            "links?:/" "/[^ \t]+ \\([^\\)]+\\)|"
            "links?:/" "/[^ \t,;:<]+"
            ")"
        );
        const xs_str *v2;
        xs *s = xs_str_new(NULL);

        xs_list_foreach(l, v2) {
            if ((n & 0x1)) {
                if (xs_startswith(v2, "img:") || xs_startswith(v2, "thumb:")) {
                    xs *l2 = xs_split(v2, "/");
                    xs *s2 = xs_fmt("<img src=\"%s%s/%s\" alt=\"\"/>",
                        xs_dict_get(sutter->config, "baseurl"), prefix, xs_list_get(l2, 2));

                    if (xs_list_len(l2) == 3)
                        s = xs_str_cat(s, s2);
                    else {
                        /* image has a class */
                        xs *s3 = xs_fmt("<div class=\"%s\">%s</div>",
                            xs_list_get(l2, 3), s2);

                        s = xs_str_cat(s, s3);
                    }

                    /* store somewhere that the image has been used */
                    /* ... */
                }
                else
                if (xs_startswith(v2, "story:")) {
                    xs *s1 = xs_replace(v2, "story:/" "/", "");
                    xs *s2 = NULL;

                    if (xs_endswith(s1, ")")) {
                        /* it has a label */
                        xs *l3 = xs_split_n(s1, " ", 1);
                        xs *lbl = xs_strip_chars_i(xs_dup(xs_list_get(l3, 1)), "()");
                        s2 = xs_fmt("<a href=\"/%s.html\">%s</a>",
                            xs_list_get(l3, 0), lbl);
                    }
                    else {
                        s2 = xs_fmt("<a href=\"/%s.html\">%s</a>", s1, s1);
                    }

                    s = xs_str_cat(s, s2);
                }
                else
                if (xs_startswith(v2, "topic:")) {
                    xs *s1 = xs_replace(v2, "topic:/" "/", "");
                    xs *s2 = xs_fmt("<a href=\"/%s/\">%s</a>", s1, s1);

                    s = xs_str_cat(s, s2);
                }
                else
                if (xs_startswith(v2, "link")) {
                    xs *s1 = xs_replace(v2, "links:", "https:");
                    s1 = xs_replace_i(s1, "link:", "http:");

                    if (xs_endswith(s1, ")")) {
                        /* it has a label */
                        xs *l3 = xs_split_n(s1, " ", 1);
                        xs *lbl = xs_strip_chars_i(xs_dup(xs_list_get(l3, 1)), "()");
                        xs *s2 = xs_fmt("<a href=\"%s\">%s</a>", xs_list_get(l3, 0), lbl);

                        s = xs_str_cat(s, s2);
                    }
                    else {
                        /* use the URL as the label */
                        xs *s2 = xs_fmt("<a href=\"%s\">%s</a>", s1, s1);
                        s = xs_str_cat(s, s2);
                    }
                }
                else
                    s = xs_str_cat(s, v2);
            }
            else
                /* store as is */
                s = xs_str_cat(s, v2);

            n++;
        }

        nc = xs_list_append(nc, s);
    }

    return nc;
}


/** renderers **/

static void raw_html_to_www(sutter *sutter, const xs_dict *entry)
{
    xs *s = xs_join(xs_dict_get(entry, "content"), "\n");
    s     = xs_str_cat(s, "\n");

    xs *fn = xs_fmt("%s%s.html", sutter->wwwdir, xs_dict_get(entry, "_id"));
    sutter_write_entry(sutter, entry, fn, s);
}


static xs_str *_html_page(sutter *sutter, const xs_dict *entry)
/* generates a base HTML page */
{
    const char *lang = xs_dict_get_def(entry, "lang", "");
    xs_str *s  = xs_str_new(NULL);
    xs *grav   = NULL;

    /* page */
    s = xs_str_cat(s, html_page);
    s = xs_replace_i(s, "<!-- entry_title -->", xs_dict_get(entry, "title"));
    s = xs_replace_i(s, "<!-- lang -->", lang);

    const xs_list *keywords = xs_dict_get(entry, "keywords");
    xs *kh = NULL;

    if (keywords != NULL) {
        xs *s = xs_join(keywords, ", ");
        kh = xs_fmt("<meta name=\"keywords\" content=\"%s\"/>\n", s);
    }
    else
        kh = xs_str_new(NULL);

    s = xs_replace_i(s, "<!-- keywords -->", kh);

    /* author header */
    xs *ah = xs_dup(html_author_header_template);

    /* get author data from the entry */
    const char *an = xs_dict_get(entry, "author_name");
    const char *aa = xs_dict_get(entry, "author_avatar");

    if (xs_is_null(an)) {
        /* no name in the entry? get the default one */
        an = xs_dict_get(sutter->config, "author_name");
        aa = xs_dict_get(sutter->config, "author_avatar");
    }
    else {
        /* there was a name in the entry, but avatar? */
        if (xs_is_null(aa)) {
            /* no; build a gravatar */
            const char *ae = xs_dict_get(entry, "author_email");

            if (!xs_is_null(ae)) {
                xs *s1 = xs_md5_hex(ae, strlen(ae));
                grav = xs_fmt("https:/" "/www.gravatar.com/avatar/%s", s1);
                aa = grav;
            }
            else
                aa = "";
        }
    }

    const char *minimum_date = xs_dict_get_def(sutter->config, "minimum_date", "");
    const char *date = xs_dict_get(entry, "date");

    if (strcmp(date, minimum_date) > 0) {
        xs *pubdate = xs_replace_i(xs_replace(date, "T", " "), "Z", "");
        xs *s_date  = xs_crop_i(xs_dup(date), 0, 10);
        ah = xs_replace_i(ah, "<!-- pubdate -->", pubdate);
        ah = xs_replace_i(ah, "<!-- short_pubdate -->", s_date);
    }
    else {
        /* entries below minimum date doesn't show a date */
        ah = xs_replace_i(ah, "<!-- pubdate -->", "");
        ah = xs_replace_i(ah, "<!-- short_pubdate -->", "&nbsp;");
    }

    s = xs_replace_i(s, "<!-- author_header -->", ah);
    s = xs_replace_i(s, "<!-- author_name -->",   an);
    s = xs_replace_i(s, "<!-- author_avatar -->", aa);

    /* add a fediverse:creator meta tag */
    const char *author_fediverse = xs_dict_get_def(entry, "author_fediverse", "");
    if (!*author_fediverse)
        author_fediverse = xs_dict_get_def(sutter->config, "author_fediverse", "");
    xs *fedi = NULL;

    if (*author_fediverse)
        fedi = xs_fmt("<meta name=\"fediverse:creator\" content=\"%s\"/>\n", author_fediverse);
    else
        fedi = xs_str_new(NULL);

    s = xs_replace_i(s, "<!-- more_meta -->", fedi);

    return s;
}


static xs_str *_append_html_page_footer(xs_str *s, const xs_dict *entry)
{
    const char *discuss = xs_dict_get(entry, "discuss");

    if (xs_is_string(discuss)) {
        xs *s1 = xs_fmt("<p><a href=\"%s\">%s</a>\n", discuss, L("Discuss"));
        s = xs_str_cat(s, s1);
    }

    return xs_str_cat(s, html_page_footer);
}


static void html_to_www(sutter *sutter, const xs_dict *entry)
{
    xs *s = NULL;

    s = _html_page(sutter, entry);

    xs *content = special_uris(sutter, xs_dict_get(entry, "content"), xs_dict_get(entry, "_prefix"));

    xs *body = xs_join(content, "\n");

    s = xs_str_cat(s, body);

    s = _append_html_page_footer(s, entry);

    xs *fn = xs_fmt("%s%s.html", sutter->wwwdir, xs_dict_get(entry, "_id"));
    sutter_write_entry(sutter, entry, fn, s);
}


static void gemini_to_www(sutter *sutter, const xs_dict *entry)
{
    xs *s = NULL;

    s = _html_page(sutter, entry);

    xs *content = _gemini_to_html(xs_dict_get(entry, "content"),
                    xs_dict_get(sutter->config, "baseurl"),
                    xs_dict_get(entry, "_prefix"));

    xs *body = xs_join(content, "\n");

    s = xs_str_cat(s, body);

    s = _append_html_page_footer(s, entry);

    xs *fn = xs_fmt("%s%s.html", sutter->wwwdir, xs_dict_get(entry, "_id"));
    sutter_write_entry(sutter, entry, fn, s);
}


static void markdown_to_www(sutter *sutter, const xs_dict *entry)
{
    xs *s = NULL;

    s = _html_page(sutter, entry);

    xs *content1 = _markdown_to_html(xs_dict_get(entry, "content"), 0);
    xs *content2 = special_uris(sutter, content1, xs_dict_get(entry, "_prefix"));

    xs *body = xs_join(content2, "\n");

    s = xs_str_cat(s, body);

    s = _append_html_page_footer(s, entry);

    xs *fn = xs_fmt("%s%s.html", sutter->wwwdir, xs_dict_get(entry, "_id"));
    sutter_write_entry(sutter, entry, fn, s);
}


static void gemini_to_gemini(sutter *sutter, const xs_dict *entry)
{
    const char *aname = NULL;
    xs *date    = NULL;
    xs *body    = NULL;
    xs *s       = NULL;

    if (xs_is_null(aname = xs_dict_get(entry, "author_name")))
        aname = xs_dict_get(sutter->config, "author_name");

    date = xs_crop_i(xs_dup(xs_dict_get(entry, "date")), 0, 10);

    xs *content1 = _gemini_to_gemini(xs_dict_get(entry, "content"));

    body = xs_join(content1, "\n");
    body = xs_strip_i(body);

    const char *minimum_date = xs_dict_get_def(sutter->config, "minimum_date", "");

    if (strcmp(date, minimum_date) > 0)
        s = xs_fmt("# %s\n\n%s %s\n\n", xs_dict_get(entry, "title"), date, aname);
    else
        s = xs_fmt("# %s\n\n%s\n\n", xs_dict_get(entry, "title"), aname);

    s = xs_str_cat(s, body);

    const char *discuss = xs_dict_get(entry, "discuss");

    if (xs_is_string(discuss))
        s = xs_str_cat(s, "\n\n=> ", discuss, " ", L("Discuss"), "\n");
    else
        s = xs_str_cat(s, "\n");

    s = xs_str_cat(s, gemini_footer);

    xs *fn = xs_fmt("%s%s.gmi", sutter->gmidir, xs_dict_get(entry, "_id"));
    sutter_write_entry(sutter, entry, fn, s);
}


static void raw_gemini_to_gemini(sutter *sutter, const xs_dict *entry)
{
    xs *s = xs_join(xs_dict_get(entry, "content"), "\n");
    s     = xs_str_cat(s, "\n");

    xs *fn = xs_fmt("%s%s.gmi", sutter->gmidir, xs_dict_get(entry, "_id"));
    sutter_write_entry(sutter, entry, fn, s);
}


/** implementation **/

void entry_render(sutter *sutter, const xs_dict *entry)
/* renders an entry */
{
    const char *fn = xs_dict_get(entry, "_fn");

    if (xs_endswith(fn, ".raw_html"))
        raw_html_to_www(sutter, entry);
    else
    if (xs_endswith(fn, ".raw_gmi"))
        raw_gemini_to_gemini(sutter, entry);
    else
    if (xs_endswith(fn, ".html"))
        html_to_www(sutter, entry);
    else
    if (xs_endswith(fn, ".md"))
        markdown_to_www(sutter, entry);
    else
    if (xs_endswith(fn, ".gmi")) {
        gemini_to_gemini(sutter, entry);
        gemini_to_www(sutter, entry);
    }
}


void entry_process(sutter *sutter, const char *fn)
/* renders or links a file from the src to the destinations */
{
    xs *entry = entry_load(sutter, fn);

    if (entry != NULL)
        entry_render(sutter, entry);
    else {
        /* hard-link to destinations */
        xs *www_fn = xs_replace(fn, sutter->srcdir, sutter->wwwdir);
        create_full_path(www_fn);
        unlink(www_fn);
        link(fn, www_fn);

        delete_target(sutter, www_fn);

        xs *gmi_fn = xs_replace(fn, sutter->srcdir, sutter->gmidir);
        create_full_path(gmi_fn);
        unlink(gmi_fn);
        link(fn, gmi_fn);

        delete_target(sutter, gmi_fn);

        sutter_debug(sutter, 1, xs_fmt("linked %s", fn));
    }
}


void render_all_serial(sutter *sutter)
/* renders everything, sequentially */
{
    /* read all entries */
    const xs_val *v;

    xs_list_foreach(sutter->sources, v)
        entry_process(sutter, v);

    render_indexes(sutter);

    target_cleanup(sutter);
}


void render_indexes(sutter *sutter)
/* renders the indexes (archive.html, rss.xml...) */
{
    int rss_cnt = 0;
    char w_year[16] = {0};
    char g_year[16] = {0};
    char date[16] = {0};
    const char *baseurl = xs_dict_get(sutter->config, "baseurl");
    const char *min_date = xs_dict_get_def(sutter->config, "minimum_date", "");
    xs *now = xs_str_utctime(0, "%Y-%m-%dT%H:%M:%SZ");
    int num_rss_entries = xs_number_get(xs_dict_get_def(sutter->config, "num_rss_entries", "10"));

    xs *idx = index_read(sutter);

    xs *archive_html = xs_list_new();
    xs *archive_gmi  = xs_list_new();

    /* RSS 2.0 */
    xs_html *rss = xs_html_tag("rss",
        xs_html_attr("xmlns:content", "http:/" "/purl.org/rss/1.0/modules/content/"),
        xs_html_attr("version",       "2.0"),
        xs_html_attr("xmlns:atom",    "http:/" "/www.w3.org/2005/Atom"));

    xs *rss_title = xs_fmt("%s - %s",
        xs_dict_get(sutter->config, "author_name"),
        xs_dict_get(sutter->config, "title"));
    xs *rss_link = xs_fmt("%s/rss.xml", baseurl);

    xs_html *channel = xs_html_tag("channel",
        xs_html_tag("title",
            xs_html_text(rss_title)),
        xs_html_tag("language",
            xs_html_text("en")),
        xs_html_tag("link",
            xs_html_text(rss_link)),
        xs_html_tag("description", NULL),
        xs_html_sctag("atom:link",
            xs_html_attr("href", rss_link),
            xs_html_attr("rel",  "self"),
            xs_html_attr("type", "application/rss+xml")));

    xs_html_add(rss, channel);

    {
        /* archive header */
        xs *s = xs_str_new(html_page);
        s = xs_replace_i(s, "<!-- entry_title -->", L("Archive"));
        s = xs_replace_i(s, "<!-- lang -->", "");
        s = xs_replace_i(s, "<!-- author_header -->", "");

        archive_html = xs_list_append(archive_html, s);
    }

    {
        /* gemini index header */
        xs *s = xs_fmt("# %s - %s\n",
            xs_dict_get(sutter->config, "title"),
            xs_dict_get(sutter->config, "author_name"));

        archive_gmi = xs_list_append(archive_gmi, s);
    }

    xs_html *sitemap_xml = xs_html_tag("urlset",
        xs_html_attr("xmlns",   "http:/" "/www.sitemaps.org/schemas/sitemap/0.9"));

    /* iterate all entries */
    const xs_str *v;

    xs_list_foreach(idx, v) {
        xs *l = xs_split(v, "\t");

        const char *i_date  = xs_list_get(l, 0);
        const char *i_title = xs_list_get(l, 1);
        const char *i_fn    = xs_list_get(l, 2);

        /* newer than now? skip */
        if (strcmp(i_date, now) > 0)
            continue;

        /* below the minimum date? generate no more */
        if (strcmp(i_date, min_date) < 0)
            break;

        xs *id = xs_replace(i_fn, sutter->wwwdir, "");
        id = xs_replace_i(id, sutter->gmidir, "");

        if (xs_startswith(i_fn, sutter->wwwdir) && rss_cnt <= num_rss_entries) {
            /* add an RSS entry */
            FILE *f;

            if ((f = fopen(i_fn, "r")) != NULL) {
                /* read the full entry from the generated file LIKE AN ANIMAL */
                xs *fe = xs_readall(f);
                fclose(f);

                int start = xs_str_in(fe, "<article ");
                int end   = xs_str_in(fe, "</article>");

                fe[end] = '\0';
                char *content = fe + start;

                /* convert the date */
                time_t t = xs_parse_time(i_date, "%Y-%m-%dT%H:%M:%S", 0);
                xs *rss_date = xs_str_utctime(t, "%a, %d %b %Y %T +0000");

                /* if it's the first entry, add this date as the date of the feed */
                if (rss_cnt == 0) {
                    xs_html_add(channel,
                        xs_html_tag("pubDate",
                            xs_html_text(rss_date)));
                }

                xs *i_link = xs_fmt("%s%s", baseurl, id);
                xs *aemail = xs_fmt("%s (%s)",
                    xs_dict_get(sutter->config, "author_email"),
                    xs_dict_get(sutter->config, "author_name"));

                xs_html *item = xs_html_tag("item",
                    xs_html_tag("title",
                        xs_html_text((char *)i_title)),
                    xs_html_tag("link",
                        xs_html_text(i_link)),
                    xs_html_tag("guid",
                        xs_html_text(i_link)),
                    xs_html_tag("pubDate",
                        xs_html_text(rss_date)),
                    xs_html_tag("author",
                        xs_html_text(aemail)),
                    xs_html_tag("description",
                        xs_html_text(content)));

                xs_html_add(channel, item);

                rss_cnt++;
            }
        }

        strncpy(date, i_date, 10);

        if (xs_startswith(i_fn, sutter->wwwdir)) {
            if (strncmp(i_date, w_year, 4) != 0) {
                /* change of year */
                strncpy(w_year, i_date, 4);

                xs *s = xs_fmt("<h2>%s</h2>\n", w_year);

                archive_html = xs_list_append(archive_html, s);
            }

            xs *s = xs_fmt("<p><a href=\"%s\">%s</a><br/>"
                        "<time class=\"sutter-pubdate\">%s</time></p>\n",
                        id, i_title[0] ? i_title : id, date);

            archive_html = xs_list_append(archive_html, s);

            xs *smurl = xs_fmt("%s%s", baseurl, id);

            xs_html_add(sitemap_xml,
                xs_html_tag("url",
                    xs_html_tag("loc",
                        xs_html_text(smurl))));
        }

        if (xs_startswith(i_fn, sutter->gmidir)) {
            if (strncmp(i_date, g_year, 4) != 0) {
                /* change of year */
                strncpy(g_year, i_date, 4);

                xs *s = xs_fmt("\n## %s\n\n", g_year);

                archive_gmi = xs_list_append(archive_gmi, s);
            }

            xs *s = xs_fmt("=> .%s %s - %s\n", id, date, i_title);

            archive_gmi = xs_list_append(archive_gmi, s);
        }
    }

    archive_html = xs_list_append(archive_html, html_page_footer);

    {
        xs *fn = xs_fmt("%s/archive.html", sutter->wwwdir);
        xs *s1 = xs_join(archive_html, "");

        write_file_if_diff(fn, s1);
        delete_target(sutter, fn);
    }
    {
        xs *fn = xs_fmt("%s/rss.xml", sutter->wwwdir);
        xs *s1 = xs_html_render_s(rss,
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");

        write_file_if_diff(fn, s1);
        delete_target(sutter, fn);
    }
    {
        xs *fn = xs_fmt("%s/archive.gmi", sutter->gmidir);
        xs *s1 = xs_join(archive_gmi, "");

        write_file_if_diff(fn, s1);
        delete_target(sutter, fn);
    }

    {
        xs *fn = xs_fmt("%s/sitemap.xml", sutter->wwwdir);
        xs *s1 = xs_html_render_s(sitemap_xml,
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");

        write_file_if_diff(fn, s1);
        delete_target(sutter, fn);
    }
}


int file_render_to_www(const char *fn)
/* renders directly any file to html (without sutter infrastructure) */
{
    FILE *f;
    int ret = 1;
    xs *s = NULL;

    if (xs_endswith(fn, ".md") || xs_endswith(fn, ".gmi")) {
        if ((f = fopen(fn, "r")) != NULL) {
            xs *c1 = xs_replace_i(xs_readall(f), "\r", "");
            fclose(f);

            xs *l1 = xs_split(c1, "\n");
            xs *l2 = NULL;

            if (xs_endswith(fn, ".md"))
                l2 = _markdown_to_html(l1, 1);
            else
                l2 = _gemini_to_html(l1, "", "");

            s = xs_join(l2, "\n");
        }
        else
            ret = 2;
    }

    if (s != NULL) {
        ret = 0;
        s = xs_str_wrap_i("<!DOCTYPE html>\n<html><head>"
                      "<meta name=\"generator\" content=\"sutter\"/>\n"
                      "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>\n"
                      "<!-- css --><!-- title --><body><!-- style -->",
                      s, "</body></html>");

        /* if there is an h1 header, use as HTML title */
        xs *l = xs_regex_select_n(s, "<h1>.+</h1>", 1);
        if (l && xs_list_len(l)) {
            xs *title = xs_dup(xs_list_get(l, 0));

            /* as regex is 'greedy', cut in the first /h1 */
            int i = xs_str_in(title, "</h1>");
            if (i != -1) {
                title[i] = '\0';

                /* destroy all tags */
                title = xs_regex_replace_i(title, "<[^>]+>", "");

                /* build tag */
                xs *title_tag = xs_fmt("<title>%s</title>", title);

                /* replace */
                s = xs_replace_i(s, "<!-- title -->", title_tag);
            }
        }

        printf("%s", s);
    }

    return ret;
}
