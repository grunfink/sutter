/* sutter - A static blog generator */
/* copyright (c) 2023 - 2025 grunfink - MIT license */

#define XS_IMPLEMENTATION

#include "xs.h"
#include "xs_hex.h"
#include "xs_io.h"
#include "xs_unicode_tbl.h"
#include "xs_unicode.h"
#include "xs_json.h"
#include "xs_time.h"
#include "xs_regex.h"
#include "xs_md5.h"
#include "xs_match.h"
#include "xs_mime.h"
#include "xs_btree.h"
#include "xs_html.h"

#include "sutter.h"


void sutter_log(sutter *sutter, xs_str *str)
/* logs a debug message */
{
    if (xs_str_in(str, sutter->basedir) != -1) {
        /* replace basedir with ~ */
        str = xs_replace_i(str, sutter->basedir, "~");
    }

    xs *tm = xs_str_localtime(0, "%H:%M:%S");
    fprintf(stderr, "%s %s\n", tm, str);

    xs_free(str);
}
