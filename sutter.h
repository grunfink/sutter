/* sutter - A static blog generator */
/* copyright (c) 2023 - 2025 grunfink - MIT license */

#define VERSION "1.06-dev"

typedef struct _sutter {
    xs_str *basedir;            /* base directory */
    xs_dict *config;            /* content of config file */
    xs_str *srcdir;             /* expanded source directory */
    xs_str *wwwdir;             /* expanded WWW output directory */
    xs_str *gmidir;             /* expanded Gemini output directory */
    xs_list *sources;           /* list of document sources */
    xs_dict *targets;           /* dict of currently existing targets */
    int sources_ctx;            /* list context for iterating sources */
    void *index;                /* xs_btree sorted index of generated documents */
    int dbglevel;               /* debugging level */
    int ok;                     /* 1 if this struct is usable */
} sutter;

#define L(s) (s)

void sutter_log(sutter *sutter, xs_str *str);
#define sutter_debug(sutter, level, str) do { if (sutter->dbglevel >= (level)) { \
    sutter_log(sutter, (str)); } } while(0)

double mtime_nl(const char *fn, int *n_link);
#define mtime(fn) mtime_nl(fn, NULL)
xs_val *rglob(xs_val *store, const char *dir);
void create_full_path(const char *fn);
int write_file_if_diff(const char *fn, const char *content);

xs_dict *entry_load(sutter *sutter, const char *fn);

int sutter_open(sutter *sutter, const char *basedir);
void sutter_close(sutter *sutter);

void prepare_templates(sutter *sutter);
void entry_process(sutter *sutter, const char *fn);

void render_all_serial(sutter *sutter);

void render_indexes(sutter *sutter);

void render_all_parallel(sutter *sutter);

void index_add(sutter *sutter, const xs_dict *entry, const char *fn);
xs_list *index_read(sutter *sutter);

int sutter_init(const char *basedir);

int file_render_to_www(const char *fn);

void delete_target(sutter *sutter, const char *fn);
void target_cleanup(sutter *sutter);
