#!/usr/bin/env python3

import sys, pygruta, os

# converts a Pygruta site to a Sutter one

gruta = pygruta.open(sys.argv[1])
outdir = sys.argv[2]

for s in gruta.story_set():
    story = gruta.story(s[0], s[1])

    format = story.get("format")

    if format == "grutatxt":
        format = "md"
    if format == "raw_html":
        format = "html"
    if format == "text":
        format = "txt"
    if format == "gemini":
        format = "gmi"

    topic = s[0]
    dir = "%s/%s" % (outdir, topic)

    try:
        os.mkdir(dir)
    except:
        pass

    with open("%s/%s.%s" % (dir, s[1], format), "w") as f:
        f.write(story.get("content"))
        f.write("\n__META__\n")

        date = story.get("date")
        n_date = date[0:4] + "-" + date[4:6] + "-" + \
            date[6:8] + "T" + date[8:10] + ":" + date[10:12] + ":" + date[12:14] + "Z"

        f.write("date: %s\n" % n_date)
        f.write("title: %s\n" % story.get("title"))
        f.write("lang: %s\n" % story.get("lang"))

        t = story.get("tags")
        if len(t):
            f.write("keywords: %s\n" % ",".join(t))
