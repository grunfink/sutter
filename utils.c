/* sutter - A static blog generator */
/* copyright (c) 2023 - 2025 grunfink - MIT license */

#include "xs.h"
#include "xs_io.h"
#include "xs_json.h"

#include "sutter.h"

#include <sys/stat.h>

const char *default_config = "{"
    "\"baseurl\":       \"\","
    "\"title\":         \"\","
    "\"cssurls\":       [ \"style.css\" ],"
    "\"dbglevel\":      0,"
    "\"author_name\":   \"\","
    "\"author_email\":  \"\","
    "\"author_avatar\": \"\","
    "\"author_fediverse\": \"\","
    "\"num_rss_entries\": 10,"
    "\"minimum_date\":  \"1998-12-31\""
"}";

const char *sutter_css = ""
    ".sutter-avatar { float: left; max-width: 40px; padding: 0.25em }\n"
    ".sutter-author { font-size: 90% }\n"
    ".sutter-pubdate { color: #a0a0a0; font-size: 90% }\n"
"";

int sutter_init(const char *basedir)
{
    FILE *f;
    xs *sutter_basedir = NULL;
    xs *config = NULL;

    if (basedir == NULL) {
        printf("Base directory:\n");
        sutter_basedir = xs_strip_i(xs_readline(stdin));
    }
    else
        sutter_basedir = xs_str_new(basedir);

    if (sutter_basedir == NULL || *sutter_basedir == '\0')
        return 1;

    sutter_basedir = xs_rstrip_chars_i(sutter_basedir, "/");

    if (mtime(sutter_basedir) != 0.0) {
        fprintf(stderr, "ERROR: directory '%s' must not exist\n", sutter_basedir);
        return 1;
    }

    config = xs_json_loads(default_config);

    printf("Blog title:\n");
    {
        xs *s = xs_strip_i(xs_readline(stdin));
        if (*s == '\0')
            return 1;

        config = xs_dict_set(config, "title", s);
    }

    printf("Blog base URL:\n");
    {
        xs *s = xs_strip_i(xs_readline(stdin));
        if (*s == '\0')
            return 1;

        config = xs_dict_set(config, "baseurl", s);
    }

    printf("Author name:\n");
    {
        xs *s = xs_strip_i(xs_readline(stdin));
        if (*s == '\0')
            return 1;

        config = xs_dict_set(config, "author_name", s);
    }

    printf("Author email:\n");
    {
        xs *s = xs_strip_i(xs_readline(stdin));
        if (*s == '\0')
            return 1;

        config = xs_dict_set(config, "author_email", s);
    }

    /* commit */

    if (mkdir(sutter_basedir, 0755) == -1) {
        fprintf(stderr, "ERROR: cannot create directory '%s'\n", sutter_basedir);
        return 1;
    }

    char *dirs[] = { "src", "www", "gmi", NULL };
    int n;

    for (n = 0; dirs[n]; n++) {
        xs *sdir = xs_fmt("%s/%s", sutter_basedir, dirs[n]);
        if (mkdir(sdir, 0755) == -1) {
            fprintf(stderr, "ERROR: cannot create subdirectory '%s'\n", sdir);
            return 1;
        }

        xs *lbl = xs_fmt("%sdir", dirs[n]);
        config = xs_dict_set(config, lbl, dirs[n]);
    }

    xs *cfg_fn = xs_fmt("%s/sutter.json", sutter_basedir);

    if ((f = fopen(cfg_fn, "w")) == NULL) {
        fprintf(stderr, "ERROR: cannot write '%s'\n", cfg_fn);
        return 1;
    }

    xs_json_dump(config, 4, f);
    fclose(f);

    xs *css_fn = xs_fmt("%s/sutter.css", sutter_basedir);

    if ((f = fopen(css_fn, "w")) == NULL) {
        fprintf(stderr, "ERROR: cannot write '%s'\n", css_fn);
        return 1;
    }

    fprintf(f, "%s", sutter_css);
    fclose(f);

    return 0;
}
