/* copyright (c) 2022 - 2025 grunfink et al. / MIT license */

#ifndef _XS_BTREE_H

#define _XS_BTREE_H

typedef struct xs_btree xs_btree;

struct xs_btree {
    xs_btree *left;     /* left branch */
    xs_btree *right;    /* right branch */
    int count;          /* # of nodes in this subtree (including this) */
    xs_keyval keyval[]; /* the key/value pair */
};

#define xs_btree_key(node)   (xs_keyval_key((node)->keyval))
#define xs_btree_value(node) (xs_keyval_value((node)->keyval))

 int xs_btree_count(const xs_btree *n);
 xs_btree *xs_btree_set(xs_btree *root, const char *key, const xs_val *val);
 const xs_btree *xs_btree_get(const xs_btree *root, const char *key);
 xs_btree *xs_btree_del(xs_btree *root, const char *key);
 const xs_btree *xs_btree_by_index(const xs_btree *root, int index);
 void xs_btree_traverse(const xs_btree *node, void (*func)(const xs_btree *, void *), void *ptr);
 void xs_btree_traverse_bw(const xs_btree *node, void (*func)(const xs_btree *, void *), void *ptr);
 void xs_btree_to_dict(const xs_btree *node, xs_dict **dict);
 void xs_btree_to_lists(const xs_btree *node, xs_list **keys, xs_list **values);
 void xs_btree_destroy(xs_btree *root);
 xs_btree *xs_dict_to_btree(const xs_dict *dict, xs_btree *root);

typedef struct xs_btree_iter {
    int dir;                    /* direction (-1, back; 1, forward) */
    int idx;                    /* index to stack */
    const xs_btree *stack[256]; /* stack of nodes */
} xs_btree_iter;

xs_btree_iter xs_btree_iter_new(const xs_btree *r, int dir);
const xs_btree *xs_btree_iter_next(xs_btree_iter *i);


#ifdef XS_IMPLEMENTATION


int xs_btree_count(const xs_btree *n)
/* returns the count of this subtree */
{
    return n ? n->count : 0;
}


xs_btree *_xs_btree_alloc(const char *key, const xs_val *val, xs_btree *l, xs_btree *r)
/* allocates a new node */
{
    xs_btree *n = xs_realloc(NULL, sizeof(xs_btree) + xs_keyval_size(key, val));

    n->left  = l;
    n->right = r;
    n->count = xs_btree_count(l) + xs_btree_count(r) + 1;

    xs_keyval_make(n->keyval, key, val);

    return n;
}


xs_btree *_xs_btree_new_left(xs_btree *root, xs_btree *branch)
{
    xs_btree *nroot = NULL;

    if (branch && branch->count > (root->count / 2) + 1) {
        /* rotate */
        xs_btree *obranch = NULL;

        obranch = _xs_btree_alloc(xs_btree_key(root), xs_btree_value(root),
                    branch->right, root->right);

        nroot = _xs_btree_alloc(xs_btree_key(branch), xs_btree_value(branch),
                    branch->left, obranch);

        xs_free(branch);
    }
    else {
        nroot = _xs_btree_alloc(xs_btree_key(root), xs_btree_value(root),
                    branch, root->right);
    }

    xs_free(root);
    return nroot;
}


xs_btree *_xs_btree_new_right(xs_btree *root, xs_btree *branch)
{
    xs_btree *nroot = NULL;

    if (branch && branch->count > (root->count / 2) + 1) {
        /* rotate */
        xs_btree *obranch = NULL;

        obranch = _xs_btree_alloc(xs_btree_key(root), xs_btree_value(root),
                    root->left, branch->left);

        nroot = _xs_btree_alloc(xs_btree_key(branch), xs_btree_value(branch),
                    obranch, branch->right);

        xs_free(branch);
    }
    else {
        nroot = _xs_btree_alloc(xs_btree_key(root), xs_btree_value(root),
                    root->left, branch);
    }

    xs_free(root);
    return nroot;
}


xs_btree *_xs_btree_merge(xs_btree *left, xs_btree *right)
{
    xs_btree *nroot;

    if (left) {
        if (right) {
            if (left->count > right->count) {
                nroot = _xs_btree_new_right(left,
                    _xs_btree_merge(left->right, right));
            }
            else {
                nroot = _xs_btree_new_left(right,
                    _xs_btree_merge(left, right->left));
            }
        }
        else
            nroot = left;
    }
    else
        nroot = right;

    return nroot;
}


xs_btree *xs_btree_set(xs_btree *root, const char *key, const xs_val *val)
/* adds a key/val to the tree */
{
    xs_btree *nroot = NULL;

    if (root) {
        int c = strcmp(key, xs_btree_key(root));

        if (c < 0) {
            nroot = _xs_btree_new_left(root, xs_btree_set(root->left, key, val));
        }
        else
        if (c > 0) {
            nroot = _xs_btree_new_right(root, xs_btree_set(root->right, key, val));
        }
        else {
            /* this same key: create a new node and destroy the old one */
            nroot = _xs_btree_alloc(key, val, root->left, root->right);
            xs_free(root);
        }
    }
    else
        /* new node */
        nroot = _xs_btree_alloc(key, val, NULL, NULL);

    return nroot;
}


const xs_btree *xs_btree_get(const xs_btree *root, const char *key)
/* returns the node that contains the key */
{
    const xs_btree *node = root;

    if (root) {
        int c = strcmp(key, xs_btree_key(root));

        if (c < 0)
            node = xs_btree_get(root->left, key);
        else
        if (c > 0)
            node = xs_btree_get(root->right, key);
    }

    return node;
}


xs_btree *xs_btree_del(xs_btree *root, const char *key)
/* deletes a key/value pair */
{
    xs_btree *nroot = root;

    if (root) {
        int c = strcmp(key, xs_btree_key(root));

        if (c < 0) {
            nroot = _xs_btree_new_left(root, xs_btree_del(root->left, key));
        }
        else
        if (c > 0) {
            nroot = _xs_btree_new_right(root, xs_btree_del(root->right, key));
        }
        else {
            nroot = _xs_btree_merge(root->left, root->right);
            xs_free(root);
        }
    }

    return nroot;
}


const xs_btree *xs_btree_by_index(const xs_btree *root, int index)
/* returns a node by index (as if it was a list) */
{
    const xs_btree *node = root;

    if (index < 0) {
        if ((index = xs_btree_count(root) + index) < 0)
            return NULL;
    }

    if (root) {
        if (index < node->count) {
            int c = xs_btree_count(root->left);

            if (index < c)
                node = xs_btree_by_index(root->left, index);
            else
            if (index > c)
                node = xs_btree_by_index(root->right, index - c - 1);
        }
        else
            node = NULL;
    }

    return node;
}


void xs_btree_traverse(const xs_btree *node, void (*func)(const xs_btree *, void *), void *ptr)
/* traverses a btree via a callback */
{
    if (node) {
        xs_btree_traverse(node->left, func, ptr);
        func(node, ptr);
        xs_btree_traverse(node->right, func, ptr);
    }
}


void xs_btree_traverse_bw(const xs_btree *node, void (*func)(const xs_btree *, void *), void *ptr)
/* traverses backwards a btree via a callback */
{
    if (node) {
        xs_btree_traverse_bw(node->right, func, ptr);
        func(node, ptr);
        xs_btree_traverse_bw(node->left, func, ptr);
    }
}


void xs_btree_to_dict(const xs_btree *node, xs_dict **dict)
/* converts a btree to a dict */
{
    xs_btree_iter i = xs_btree_iter_new(node, 0);
    const xs_btree *v;

    while ((v = xs_btree_iter_next(&i)))
        *dict = xs_dict_append(*dict, xs_btree_key(v), xs_btree_value(v));
}


void xs_btree_to_lists(const xs_btree *node, xs_list **keys, xs_list **values)
/* converts the btree to two lists */
{
    xs_btree_iter i = xs_btree_iter_new(node, 0);
    const xs_btree *v;

    while ((v = xs_btree_iter_next(&i))) {
        if (keys)
            *keys = xs_list_append(*keys, xs_btree_key(v));

        if (values)
            *values = xs_list_append(*values, xs_btree_value(v));
    }
}


void xs_btree_destroy(xs_btree *root)
/* destroy everything */
{
    if (root) {
        xs_btree_destroy(root->left);
        xs_btree_destroy(root->right);
        xs_free(root);
    }
}


xs_btree *xs_dict_to_btree(const xs_dict *dict, xs_btree *root)
/* converts a dict to a btree */
{
    const xs_str *k;
    const xs_val *v;

    xs_dict_foreach(dict, k, v)
        root = xs_btree_set(root, k, v);

    return root;
}


/** iterator **/

static void _xs_btree_iter_prepare(xs_btree_iter *i, const xs_btree *n)
{
    while (n && i->idx < (int)xs_countof(i->stack)) {
        /* pushes this tree */
        i->stack[i->idx++] = n;

        /* advances */
        n = i->dir == -1 ? n->right : n->left;
    }
}


xs_btree_iter xs_btree_iter_new(const xs_btree *r, int dir)
/* starts an iterator */
{
    xs_btree_iter i = { dir, 0, {0} };

    _xs_btree_iter_prepare(&i, r);

    return i;
}


const xs_btree *xs_btree_iter_next(xs_btree_iter *i)
/* returns next node */
{
    const xs_btree *r = NULL;

    if (i->idx) {
        /* pops from the stack */
        r = i->stack[--i->idx];

        _xs_btree_iter_prepare(i, i->dir == -1 ? r->left : r->right);
    }

    return r;
}


#endif /* XS_IMPLEMENTATION */

#endif /* XS_BTREE_H */
